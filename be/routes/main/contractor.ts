import express, { Request, Response } from 'express';
import Contractor from '../../models/contractor';

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve list of contractors / QP
// @access  Public
router.get('/', async (req: Request, res: Response): Promise<void> => {
    try {
        let contractors = await Contractor.findAll();
        if (contractors.length > 0){
            res.json(contractors);
        } else {
            res.status(404).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to locate any Contractors details",
                "detail": "There is no Contractors registered in the system",
                "instance": "Searching information for all Contractors"
            })
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   POST /submit
// @desc    Create contractor / QP profile
// @access  Public
router.post('/submit', async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;
    const name: string = req.body.name;
    const role: string = req.body.role;
    const company: string = req.body.company;
    const projectId: string = req.body.projectId;

    try {
        // Does contractor exist
        let contractor = await Contractor.findOne({ where: { id }})
        if (contractor){
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to register Contractor",
                "detail": "The Contractor is in the system already",
                "instance": "Registering a new Contractor"
            })
        } else {
            // If contractor does not exist
            let newContractor = await Contractor.create({ id, name, role, company, projectId })
            res.json(newContractor)
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   GET /delete
// @desc    Delete an contractor
// @access  Public
router.delete('/delete', async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;

    try {
        // Does contractor exist
        let contractor = await Contractor.findOne({ where: { id }})
        if (contractor){
            // Delete Contractor
            await contractor.destroy();
            res.json({ 'message': 'Delete successful'})
        } else {
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to find Company",
                "detail": "Company does not exist",
                "instance": "Deleting an Company"
            })
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        })
    }
});


export default router;