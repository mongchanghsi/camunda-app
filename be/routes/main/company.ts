import express, { Request, Response } from 'express';
import Company from '../../models/company';

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve companies
// @access  Public
router.get('/', async (req: Request, res: Response): Promise<void> => {
    try {
        let companies = await Company.findAll();
        if (companies.length > 0){
            res.json(companies);
        } else {
            res.status(404).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to locate any Companies details",
                "detail": "There is no Companies registered in the system",
                "instance": "Searching information for all Companies"
            })
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   POST /submit
// @desc    Register a company
// @access  Public
router.post('/submit', async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;
    const name: string = req.body.name;

    try {
        // Does company exist
        let company = await Company.findOne({ where: { id }})
        if (company){
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to register Company",
                "detail": "The Company is in the system already",
                "instance": "Registering a new Company"
            })
        } else {
            // If company does not exist
            let newCompany = await Company.create({ id, name })
            res.json(newCompany)
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   GET /delete
// @desc    Delete an company
// @access  Public
router.delete('/delete', async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;

    try {
        // Does company exist
        let company = await Company.findOne({ where: { id }})
        if (company){
            // Delete Company
            await company.destroy();
            res.json({ 'message': 'Delete successful'})
        } else {
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to find Company",
                "detail": "Company does not exist",
                "instance": "Deleting an Company"
            })
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});


export default router;