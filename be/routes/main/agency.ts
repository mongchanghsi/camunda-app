import express, { Request, Response } from 'express';
import Agency from '../../models/agency';

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve all agencies
// @access  Public
router.get('/', async (req: Request, res: Response): Promise<void> => {
    try {
        let agencies = await Agency.findAll();
        if (agencies.length > 0){
            res.json(agencies);
        } else {
            res.status(404).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to locate any Agencies details",
                "detail": "There is no Agencies registered in the system",
                "instance": "Searching information for all Agenciess"
            })
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }

});

// @route   GET /submit
// @desc    Submit an agency
// @access  Public
router.post('/submit',async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;
    const name: string = req.body.name;

    try {
        // Does agency exist
        let agency = await Agency.findOne({ where: { id }})
        if (agency){
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to register Agency",
                "detail": "The Agency is in the system already",
                "instance": "Registering a new Agency"
            })
        } else {
            // If agency does not exist
            let newAgency = await Agency.create({ id, name })
            res.json(newAgency)
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   GET /delete
// @desc    Delete an agency
// @access  Public
router.delete('/delete', async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;

    try {
        // Does agency exist
        let agency = await Agency.findOne({ where: { id }})
        if (agency){
            // Delete Agency
            await agency.destroy();
            res.json({ 'message': 'Delete successful'})
        } else {
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to find Agency",
                "detail": "Agency does not exist",
                "instance": "Deleting an Agency"
            })
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

export default router;