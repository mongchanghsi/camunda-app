import express, { Request, Response } from 'express';
import Task from '../../models/task';
import { camunda_getTasks, camunda_completeTask, camunda_getTaskByAssignee, camunda_getTasksByInstanceID } from '../camunda/camunda-api';

interface taskAttributes {
    taskId: string;
    taskName: string;
    assignee: string;
    completed: boolean;
    projectId: string;
}

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve current tasks (from camunda)
// @access  Public
router.get('/', async (req: Request, res: Response): Promise<void> => {
    const processDefinitionID: string = 'opp_application_v2:10:8096a847-e28b-11ea-be7c-0242ac110002';
    let taskList = await camunda_getTasks(processDefinitionID);
    res.send(taskList);
});

// @route   GET /assignee/:assignee
// @desc    Retrieve current tasks by assignee (from camunda)
// @access  Public
router.get('/assignee/:assignee', async (req: Request, res: Response): Promise<void> => {
    const processDefinitionID: string = 'opp_application_v2:10:8096a847-e28b-11ea-be7c-0242ac110002';
    const assignee: string = req.params.assignee;
    let taskList: taskAttributes[] = await camunda_getTaskByAssignee(processDefinitionID, assignee);
    if (taskList.length > 0){
        res.send(taskList);
    } else {
        res.json({'message': 'Tasks not found'})
    }
});

// @route   GET /assignee/:assignee
// @desc    Retrieve current tasks by assignee (from camunda)
// @access  Public
router.get('/assignee2/:assignee', async (req: Request, res: Response): Promise<void> => {
    const assignee: string = req.params.assignee;
    try {
        let tasks = await Task.findAll({ where: { assignee: assignee, completed: false }})
        let formattedTaskList: taskAttributes[] = [];
        if (tasks.length > 0){
            for(let i=0; i<tasks.length; i++){
                let task: taskAttributes = {
                    taskId: tasks[i].id,
                    taskName: tasks[i].name,
                    assignee: tasks[i].assignee,
                    completed: tasks[i].completed,
                    projectId: tasks[i].projectId
                }
                formattedTaskList.push(task);
            };
            res.send(formattedTaskList);
        } else {
            res.status(400).json({ 'errorCode': 400, 'message': 'No Task Found' })
        }
    }
    catch (error) {
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   GET /all
// @desc    Retrieve all tasks - done or undone (from postgres)
// @access  Public
router.get('/all', async (req: Request, res: Response): Promise<void> => {
    try {
        let tasks = await Task.findAll();
        if (tasks.length > 0){
            res.json(tasks);
        } else {
            res.status(404).json({ 'message': 'No Tasks Found' })
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   POST /complete
// @desc    Complete a task by taskId
// @access  Public
router.post('/complete/:id', async (req: Request, res: Response): Promise<void> => {
    const taskID: string = req.params.id;
    const projectId: string = req.body.projectId;
    let result = await camunda_completeTask(taskID);
    
    // Update Postgres that the task is complete
    const completedTask = await Task.findOne({ where: { id: taskID }});
    if (completedTask){
        completedTask.completed = true;
        await completedTask.save();
        console.log('Task completed');
    } else {
        res.status(400).json({ 'message': 'Task not found'});
    }

    // Fetch all camunda task with this projectId -> maybe there is a new task after the completion
    let camundaTaskList = await camunda_getTasksByInstanceID(projectId);
    let camundaTaskList_taskID: string[] = [];
    for(let i=0; i<camundaTaskList.length; i++){
        camundaTaskList_taskID.push(camundaTaskList[i].taskId)
    };
    
    // Fetch all postgres task with this projectId, filter out the not completed ones
    let postgresTaskList = await Task.findAll({ where: { projectId: projectId, completed: false }});
    let postgresTaskList_taskID: string[] = [];
    for(let i=0; i<postgresTaskList.length; i++){
        postgresTaskList_taskID.push(postgresTaskList[i].id)
    };

    if (!(camundaTaskList_taskID === postgresTaskList_taskID)) {
        // Filtering out an array of taskId that needs to be update into postgres
        let arrayOfIdToUpdate: string[] = camundaTaskList_taskID.filter(taskID => !postgresTaskList_taskID.includes(taskID));

        // Filtering out an array of task objects that needs to be update into postgres
        let arrayOfTasksToUpdate: taskAttributes[] = camundaTaskList.filter(taskObject => arrayOfIdToUpdate.includes(taskObject.taskId))

        // Storing every new task into postgres
        for(let i=0; i<arrayOfTasksToUpdate.length; i++){
            let currentTask = arrayOfTasksToUpdate[i]
            Task.create({ 
                id: currentTask.taskId,
                name: currentTask.taskName,
                projectId,
                completed: currentTask.completed,
                assignee: currentTask.assignee
            })
            .then(() => console.log(`Stored task for ${currentTask.taskId}`))
            .catch(error => console.error(error.message))
        }
    } else {
        console.log('no need to update coz no new task')
    }
});

export default router;
