import express, { Request, Response } from 'express';
import Po from '../../models/po';

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve list of PO
// @access  Public
router.get('/', async (req: Request, res: Response): Promise<void> => {
    try {
        let pos = await Po.findAll();
        if (pos.length > 0){
            res.json(pos);
        } else {
            res.status(404).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to locate any Processing Officer details",
                "detail": "There is no Processing Officer registered in the system",
                "instance": "Searching information for all Processing Officers"
            })
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   POST /submit
// @desc    Create PO profile
// @access  Public
router.post('/submit', async (req: Request, res: Response): Promise<void> => {
    const id: string = req.body.id;
    const name: string = req.body.name;
    const agencyId: string = req.body.agencyId;

    try {
        // Does Po exist
        let po = await Po.findOne({ where: { id }})
        if (po){
            res.status(400).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to register Processing Officer",
                "detail": "The Processing Officer is in the system already",
                "instance": "Registering a new Processing Officer"
            })
        } else {
            // If PO does not exist
            let newPo = await Po.create({ id, name, agencyId })
            res.json(newPo)
        }
    }
    catch (error){
        console.error(error.message)
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

export default router;