import express, { Request, Response } from 'express';
import ContractorProject from '../../models/cont-proj';

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve projects
// @access  Public
router.get('/', (req: Request, res: Response): void => {
    ContractorProject.findAll()
        .then(project => res.send(project))
        .catch(error => console.error(error.message))
});

// @route   POST /submit
// @desc    Submit a project
// @access  Public
router.post('/submit', (req: Request, res: Response): void => {
    const id: string = req.body.id;
    const contractorId: string = req.body.contractorId;
    const projectId: string = req.body.projectId;

    try {
        ContractorProject.create({ id, projectId, contractorId})
            .then(result => res.send(result))
            .catch(error => console.error(error.message))
    }
    catch(error){
        console.error(error.message)
    }
});

export default router;