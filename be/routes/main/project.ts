import express, { Request, Response } from 'express';
import Project from '../../models/project';
import Task from '../../models/task';
import { camunda_startInstance, camunda_getTasksByInstanceID } from '../camunda/camunda-api';

interface returnObjectAttributes {
    instanceId: string;
    definitionId: string;
};

interface taskAttributes {
    taskId: string;
    taskName: string;
    assignee: string;
    completed: boolean;
};

const router = express.Router({
    strict: true
});

// @route   GET /
// @desc    Retrieve all projects
// @access  Public
router.get('/', async (req: Request, res: Response): Promise<void> => {
    try {
        let projects = await Project.findAll();
        if (projects.length > 0){
            res.json(projects);
        } else {
            res.status(404).json({
                "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
                "title": "Unable to locate any Project details",
                "detail": "There is no Project registered in the system",
                "instance": "Searching information for all Projects"
            })
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

// @route   POST /submit
// @desc    Create a new project
// @access  Public
router.post('/submit', async (req: Request, res: Response): Promise<void> => {
    const name: string = req.body.name;
    const desc: string = req.body.desc;
    const companyId: string = req.body.companyId;

    try {
        // Start a process instance in camunda
        let camundaInstance: returnObjectAttributes = await camunda_startInstance();
        const projectId: string = camundaInstance.instanceId

        // Save to project table
        Project.create({ id: projectId, name, desc, companyId })
            .then(result => res.send(result))
            .catch(error => console.error(error.message)) 

        // Taking the instanceId => find camunda tasks => store initial tasks into postgres
        // Must store project first then store task if not will block by constraint
        let taskList: taskAttributes[] = await camunda_getTasksByInstanceID(projectId);
        for(let i=0; i<taskList.length; i++){
            let currentTask = taskList[i]
            Task.create({ 
                id: currentTask.taskId,
                name: currentTask.taskName,
                projectId,
                completed: currentTask.completed,
                assignee: currentTask.assignee
            })
            .then(() => console.log(`Stored task for ${currentTask.taskId}`))
            .catch(error => console.error(error.message))
        }
    }
    catch(error){
        console.error(error.message);
        res.status(500).json({
            "type": "https://gitlab.com/mongchanghsi/camunda-app/-/blob/master/be/Error%20Handling%20Codes.md",
            "title": "Internal Server Error",
            "detail": "Server faced an issue"
        });
    }
});

export default router;