import fetch from 'node-fetch';

const url = 'http://localhost:8080/engine-rest';

interface returnObjectAttributes {
    instanceId: string;
    definitionId: string;
}

interface taskAttributes {
    taskId: string;
    taskName: string;
    assignee: string;
    completed: boolean;
    projectId: string;
}

// Start a camunda process instance
export const camunda_startInstance = async (): Promise<returnObjectAttributes> => {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
    }

    let returnObject: returnObjectAttributes = {
        instanceId: '',
        definitionId: ''
    }

    try {
        await fetch(`${url}/process-definition/key/opp_application_v2/start`, requestOptions)
        .then(res => res.json())
        .then(json => {
            returnObject.instanceId = json.id;
            returnObject.definitionId = json.definitionId;
        })
        .catch(error => console.error('Error occured', error.message))
    }
    catch(error){
        console.error(error.message)
    }

    return returnObject;
};

// Retrieve all current tasks
export const camunda_getTasks = async (processDefinitionID: string): Promise<taskAttributes[]> => {
    let taskArray: taskAttributes[] = [];

    try {
        await fetch(`${url}/task?processDefinitionId=${processDefinitionID}`)
            .then(res => res.json())
            .then(json => {
                for(let i=0; i<json.length; i++){
                    let currentTask: taskAttributes = {
                        taskId: json[i].id,
                        taskName: json[i].name,
                        assignee: json[i].assignee,
                        completed: false,
                        projectId: json[i].processInstanceId
                    }
                    taskArray.push(currentTask);
                }
            })
            .catch(error => console.error('Error occured', error.message))
    }
    catch (error){
        console.error(error.message);
    }

    return taskArray;
};

// Retrieve tasks by instance id
export const camunda_getTasksByInstanceID = async (processInstanceId: string): Promise<taskAttributes[]> => {
    let taskArray: taskAttributes[] = [];

    try {
        await fetch(`${url}/task?processInstanceId=${processInstanceId}`)
            .then(res => res.json())
            .then(json => {
                for(let i=0; i<json.length; i++){
                    let currentTask: taskAttributes = {
                        taskId: json[i].id,
                        taskName: json[i].name,
                        assignee: json[i].assignee,
                        completed: false,
                        projectId: processInstanceId
                    }
                    taskArray.push(currentTask);
                }
            })
            .catch(error => console.error('Error occured', error.message))
    }
    catch (error){
        console.error(error.message);
    }

    return taskArray;
};

// Retrieve tasks by assignee
export const camunda_getTaskByAssignee = async (processDefinitionID: string, assignee: string): Promise<taskAttributes[]> => {
    let taskArray: taskAttributes[] = [];

    try {
        await fetch(`${url}/task/?processDefinitionId=${processDefinitionID}&assignee=${assignee}`)
            .then(res => res.json())
            .then(json => {
                for(let i=0; i<json.length; i++){
                    let currentTask: taskAttributes = {
                        taskId: json[i].id,
                        taskName: json[i].name,
                        assignee: json[i].assignee,
                        completed: false,
                        projectId: json[i].processInstanceId
                    }
                    taskArray.push(currentTask);
                }
            })
            .catch(error => console.error('Error occured', error.message))
    }
    catch (error){
        console.error(error.message);
    }
    return taskArray;
};

// Complete a task
export const camunda_completeTask = async (taskID: string): Promise<string> => {
    let completed: boolean = false;

    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'}
    }

    try {
        await fetch(`${url}/task/${taskID}/complete`, requestOptions)
            .then(res => res.text())
            .then(json => {
                completed = true;
            })
            .catch(error => console.error('Error occured', error))
    }
    catch (error){
        console.error(error.message);
    }
    
    if (completed){
        return 'completed'
    } else {
        return 'incomplete'
    }
};

