import { Sequelize } from 'sequelize';

let db = new Sequelize('cnx', 'postgres', 'password', {
    host: 'localhost',
    dialect: 'postgres',

    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
  });

export default db;