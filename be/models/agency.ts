import { Model, DataTypes } from 'sequelize';
import db from '../config/database';

interface agencyAttributes extends Model{
    id: string;
    name: string;
}

const Agency = db.define<agencyAttributes>('agency', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    timestamps: false
});

export default Agency;
