import { Model, DataTypes } from 'sequelize';
import db from '../config/database';

interface companyAttributes extends Model{
    id: string;
    name: string;
    uen: string;
}

const Company = db.define<companyAttributes>('company', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    uen: {
        type: DataTypes.STRING
    }
},{
    timestamps: false
})

export default Company;