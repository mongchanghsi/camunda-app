import { Model, DataTypes } from 'sequelize';
import db from '../config/database';
import Agency from './agency';

// PO for Processing Officer

interface poAttributes extends Model{
    id: string;
    name: string;
    agencyId: string;
}

const Po = db.define<poAttributes>('po', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    agencyId: {
        type: DataTypes.STRING,
        references: {
            model: Agency,
            key: 'id'
        }
    }
},{
    timestamps: false
})

export default Po;