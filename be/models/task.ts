import { Model, DataTypes } from 'sequelize';
import db from '../config/database';
import Project from './Project';
import Po from './Po';

interface taskAttributes extends Model{
    id: string;
    name: string;
    poId: string;
    projectId: string;
    completed: boolean;
    assignee: string;
};

const Task = db.define<taskAttributes>('task', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    poId: {
        type: DataTypes.STRING,
        references: {
            model: Po,
            key: 'id'
        }
    },
    projectId: {
        type: DataTypes.STRING,
        references: {
            model: Project,
            key: 'id'
        }
    },
    completed: {
        type: DataTypes.BOOLEAN
    },
    assignee: {
        type: DataTypes.STRING 
    }
},{
    timestamps: false
});

export default Task;