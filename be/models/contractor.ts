import { Model, DataTypes } from 'sequelize';
import db from '../config/database';

interface contractorAttributes extends Model{
    id: string;
    name: string;
    role: string;
    company: string;
    projectId: string;
}

const Contractor = db.define<contractorAttributes>('contractor', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    role: {
        type: DataTypes.STRING
    },
    company: {
        type: DataTypes.STRING 
    },
    projectId: {
        type: DataTypes.STRING
    }
},{
    timestamps: false
})

export default Contractor;