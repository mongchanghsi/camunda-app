import { Model, DataTypes } from 'sequelize';
import db from '../config/database';
import Contractor from './contractor';
import Project from './project';

interface cpAttributes extends Model{
    id: string;
    projectId: string;
    contractorId: string;
};

const ContractorProject = db.define<cpAttributes>('contractorproject', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    projectId: {
        type: DataTypes.STRING,
        references: {
            model: Project,
            key: 'id'
          }
    },
    contractorId: {
        type: DataTypes.STRING,
        references: {
            model: Contractor,
            key: 'id'
          }
    }
},{
    timestamps: false
});

export default ContractorProject;