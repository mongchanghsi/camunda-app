import { Model, DataTypes } from 'sequelize';
import db from '../config/database';
import Company from './Company';

interface projectAttributes extends Model{
    id: string;
    name: string;
    desc: string;
    companyId: string;
}

const Project = db.define<projectAttributes>('project', {
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    desc: {
        type: DataTypes.STRING
    },
    companyId: {
        type: DataTypes.STRING,
        references: {
            model: Company,
            key: 'id'
        },
        allowNull: false,
        validate: {
            notEmpty: true
        }
    }
},{
    timestamps: false
});

export default Project;