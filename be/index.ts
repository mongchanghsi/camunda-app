import express, { Application } from 'express';
import db from './config/database';
import cors from 'cors';

// Import Routes
import Project from './routes/main/project';
import Task from './routes/main/task';
import Po from './routes/main/po';
import Agency from './routes/main/agency';
import Company from './routes/main/company';
import Contractor from './routes/main/contractor';

// Express Set-up
const app: Application = express();
app.use(express.json());
app.use(cors());

// Postgres Set-up
db.authenticate()
  .then(() => console.log('Connected to DB'))
  .catch((error) => console.error(error.message))

// Routes
app.use('/api/project', Project);
app.use('/api/task', Task);
app.use('/api/po', Po);

// Postgres Routes
app.use('/api/agency', Agency);
app.use('/api/company', Company);
app.use('/api/contractor', Contractor);


// Start-up
const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening to port ${port}...`));