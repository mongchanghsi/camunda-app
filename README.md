## Camunda Platform

### Instructions

1. Clone the repository
2. Using ```cmd``` and navigate to the directory of the repository
3. Install dependencies using ```npm install```
4. To start the client (front-end), ```cd client``` and npm start
5. To start the apis (back-end), ```cd be``` and npm start, but before that:
   1. Must start Camunda on Ubuntu
   2. Must start Postgres
6. Client is accessible via ```http://localhost:3000/```
7. APIs are accessible via ```http://localhost:4000/api```

### Notes

1. Only project and task routes are required to interact with both Postgres and Camunda, the rest of the routes are only involved with Postgres

2. Ignore the @access because it is meant to describe whether a router is a Public route or a Private route. Since there is no auth middleware & is for testing, hence it will be Public.

3. Did not include association because it seems like it worked as long as you specify in the GUI? I tried excluding it but it still check against the constraints.

   ```javascript
   // index.ts
   // ASSOCIATION
   // Each company has many projects | Projects has only 1 company
   Company.hasMany(Project);
   Project.belongsTo(Company); // col companyId in project table
   
   // Each project has many tasks | Tasks has only 1 project
   Project.hasMany(Task);
   Task.belongsTo(Project); // col projectId in task table
   
   // Each project has many QP | Each QP can have many projects
   // Not very sure about this part
   Project.belongsToMany(Contractor, { through: ContractorProject });
   Contractor.belongsToMany(Project, { through: ContractorProject });
   
   // Each Processing Officer has many tasks | Each task has only 1 Processing Officer
   Po.hasMany(Task);
   Task.belongsTo(Po); // col poId in task table
   
   // Each Agency has many PO | Each PO has only 1 agency
   Agency.hasMany(Po);
   Po.belongsTo(Agency); // col agencyId po table
   ```

   



