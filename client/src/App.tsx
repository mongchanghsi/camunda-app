import React from 'react';
import './App.css';
import Header from './components/Header';
import Submission from './components/Submission';
import TableProject from './components/Tables/TableProject';
import TableTask from './components/Tables/TableTask';

function App() {
  return (
    <div className="App">
      <Header/>
      <Submission/>
      <TableProject/>
      <TableTask/>
    </div>
  );
}

export default App;