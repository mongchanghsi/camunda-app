// Table which contains ALL of the existing projects
import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { Typography, Paper } from '@material-ui/core';

interface projectAttributes {
    id: string;
    name: string;
    desc: string;
    companyId: string;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        margin: '30px 0 30px 0'
    },
    title: {

    },
    table_container: {
        margin: 'auto',
        width: 600
    },
    table: {
        minWidth: 500,
      },
  }),
);

function TableProject(): any {
    const classes = useStyles();

    const [projectList, setProjectList] = useState<projectAttributes[]>([]);

    const fetchList = async (): Promise<void> => {
        await fetch(`http://localhost:4000/api/project`)
            .then(res => res.json())
            .then(json => setProjectList(json))
            .catch(error => console.error('Error occured', error.message))
    };

    useEffect(() => {
        console.log('Fetching project list from postgres')
        fetchList();
    }, []);

    return (
        <div className={classes.root}>
            <Typography variant="h5" component="h2">
                On Going Projects
            </Typography>
            <TableContainer component={Paper} className={classes.table_container}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell align="center"> Project Title </TableCell>
                        <TableCell align="center"> Project ID </TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        { projectList.length > 0 ? projectList.map((project: projectAttributes) => 
                            <TableRow key={project.id}>
                                <TableCell> {project.name} </TableCell>
                                <TableCell> {project.id} </TableCell>
                            </TableRow>
                        ) : 
                            <TableRow>
                                <TableCell> No Task Available </TableCell>
                            </TableRow>
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
};

export default TableProject;