// Table which contain tasks assigned to that particular assignee
// Contains TaskItem which will be loop through as row
import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { Typography, Paper } from '@material-ui/core';
import { InputLabel, MenuItem, FormControl, Select } from '@material-ui/core';
import TaskItem from './TaskItem';

interface taskAttributes {
    taskId: string;
    taskName: string;
    assignee: string;
    completed: boolean;
    projectId: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        margin: '30px 0 30px 0'
    },
    table_container: {
        margin: 'auto',
        width: 1000
    },
    table: {
        minWidth: 1000,
      },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
  }),
);

function TableTask(): any {
    const classes = useStyles();

    const [taskList, setTaskList] = useState<taskAttributes[]>([]);
    const [user, setUser] = useState<string>('all');

    useEffect(() => {
        const fetchTaskList = async (): Promise<void> => {
            if (user === 'all'){
                await fetch(`http://localhost:4000/api/task`)
                    .then(res => res.json())
                    .then(json => setTaskList(json))
                    .catch(error => console.error('Error occured', error.message))
            } else {
                await fetch(`http://localhost:4000/api/task/assignee/${user}`)
                    .then(res => res.json())
                    .then(json => setTaskList(json))
                    .catch(error => console.error('Error occured', error.message))
            }
        };
        console.log('Fetching task list from camunda');
        fetchTaskList();
    },[user]);

    return (
        <div className={classes.root}>
            <Typography variant="h5" component="h2">
                Tasks
            </Typography>
            <FormControl className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label">Assignee</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={user}
                        onChange={(e: React.ChangeEvent<{value: unknown}>)=>{setUser(e.target.value as string)}}
                    >
                        <MenuItem value='all'>All</MenuItem>
                        <MenuItem value='demo'>Client</MenuItem>
                        <MenuItem value='nea'>NEA</MenuItem>
                        <MenuItem value='lta'>LTA</MenuItem>
                    </Select>
            </FormControl>
            <TableContainer component={Paper} className={classes.table_container}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell align="center"> Project ID </TableCell>
                        <TableCell align="center"> Task Name </TableCell>
                        <TableCell align="center"> Task ID </TableCell>
                        <TableCell align="center"> Assginee </TableCell>
                        <TableCell align="center"> Complete Task </TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        { taskList.length > 0 ? taskList.map((task: taskAttributes) => <TaskItem key={task.taskId} task={task}/>) : 
                        <TableRow>
                            <TableCell> No Task Available </TableCell>
                        </TableRow>}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
};

export default TableTask;