// For each row of data
import React, { Fragment } from 'react';
import { TableCell, TableRow, Button } from '@material-ui/core';

interface taskAttributes {
    taskId: string;
    taskName: string;
    assignee: string;
    completed: boolean;
    projectId: string;
}

function TaskItem(props: {task: taskAttributes}): any {
    const { task } = props;

    const completeTask = async (e:React.MouseEvent) => {
        e.preventDefault();

        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({ projectId: task.projectId })
        }

        await fetch(`http://localhost:4000/api/task/complete/${task.taskId}`, requestOptions)
            .then(res => res.json())
            .then(json => console.log(json))
            .catch(error => console.error('Error occured', error.message))
    };

    return (
        <Fragment>
            <TableRow>
                <TableCell align="center"> {task.projectId} </TableCell>
                <TableCell align="center"> {task.taskName} </TableCell>
                <TableCell align="center"> {task.taskId} </TableCell>
                <TableCell align="center"> {task.assignee} </TableCell>
                <TableCell align="center">
                    <Button onClick={(e) => {completeTask(e)}}> Complete Task </Button>
                </TableCell>
            </TableRow>
        </Fragment>
    )
};

export default TaskItem;