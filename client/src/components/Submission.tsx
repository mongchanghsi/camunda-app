import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Card, CardContent, Button, Typography, TextField } from '@material-ui/core';

interface projectInfo {
    title: string;
    description: string;
    companyId: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        margin: '30px 0 30px 0'
    },
    card: {
        margin: 'auto',
        width: 400
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
        margin: 'auto',
        padding: '15px 0 15px 0'
    }
  }),
);

function Submission(){
    const classes = useStyles();

    const [formData, setFormData] = useState<projectInfo>({
        title: '',
        description: '',
        companyId: '' 
    });

    const updateForm = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement >) => {
        setFormData({ ...formData, [e.target.id]: e.target.value })
    };

    const submitProject = async (e: React.MouseEvent):Promise<void> => {
        e.preventDefault();

        const { title, description, companyId } = formData;

        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({ name: title, desc: description, companyId })
        }

        await fetch(`http://localhost:4000/api/project/submit`, requestOptions)
            .then(res => res.json())
            .then(json => console.log(json))
            .catch(error => console.error('Error occured', error.message))
    };

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <CardContent>
                    <Typography variant="h5" component="h2">
                        Start a new project
                    </Typography>
                    <form className={classes.form} noValidate autoComplete="off">
                        <div className={classes.form_body}>
                            <div>
                                <TextField id="title" label="Project Title" value={formData.title} onChange={(e: React.ChangeEvent<HTMLInputElement>)=>updateForm(e)} required/>
                            </div>
                            <div>
                                <TextField id="description" label="Project Description" value={formData.description} onChange={(e: React.ChangeEvent<HTMLInputElement>)=>updateForm(e)} required/>
                            </div>
                            <div>
                                <TextField id="companyId" label="Company Id" value={formData.companyId} onChange={(e: React.ChangeEvent<HTMLInputElement>)=>updateForm(e)} required/>
                            </div>
                        </div>
                    </form>
                    <Button onClick={(e)=>submitProject(e)}> Submit </Button>
                </CardContent>
            </Card>
        </div>
    )
}

export default Submission;